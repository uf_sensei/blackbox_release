
export LD_LIBRARY_PATH=${HOME}/local/lib/:${LD_LIBRARY_PATH}

date

nodes=4
circuits=4


mpd &

killall evl 
killall gen
killall mobile

sleep 1

p1=22000
p2=23000
p3=25000

prog=./circuits/blackboxvisionhamming928x10.cir.bin.hash-free


/usr/lib64/openmpi/bin/mpirun  -n $nodes ./evl 80 $circuits $prog  inp.txt 127.0.0.1 $p3 1  $p1     &
sleep 1
/usr/lib64/openmpi/bin/mpirun  -n $nodes ./gen 80 $circuits $prog  inp.txt 127.0.0.1 $p3 1 $p2 &
sleep 1
./mobile $prog mobileinp.txt 127.0.0.1 $p1 127.0.0.1 $p2 

###
exit
###

