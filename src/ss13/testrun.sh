
export LD_LIBRARY_PATH=${HOME}/local/lib/:${LD_LIBRARY_PATH}

date

nodes=4
circuits=8


mpd &

killall evl 
killall gen

sleep 1

p1=22000
p2=23000
p3=25000

#prog=./circuits/keyed64cmtb.cir.bin.hash-free
prog=./circuits/aes.cir.bin.hash-free


mpirun  -n $nodes ./evl 80 $circuits $prog  inp.txt 127.0.0.1 $p3 1     &
sleep 1
mpirun  -n $nodes ./gen 80 $circuits $prog  inp.txt 127.0.0.1 $p3 1    

###
exit
###

